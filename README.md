# Report Generation and GGPlot

This repository contains a q script with python and ggplot wrapper functions which generate a Report PDF containing graphs, text and kdb+ data tables. 

This q script can be run on a local version of developer. See https://code.kx.com/developer/ and https://code.kx.com/q/learn/install/ if you do not have developer and q installed. For this q script to run you will need to have the following available:
- Python (https://www.python.org/downloads/)
- embedPy (https://code.kx.com/q/ml/embedpy/)
- Python packages numpy and reportlab (https://numpy.org/install/ & https://www.reportlab.com/dev/install/open_source_installation/)
- GG library available (this is already available on developer)
