// load libraries if using q without developer
//system "l /opt/developer/ws/graphics.q_";
// load and init python through embedPY
/\l p.q
// init python by calling a function
p)print(1+1)
// Define the location for storing all PNGs and PDFs
.pdf.filePath:ssr[ getenv`DEVELOPER_HOME;"\\";"/"],"/Reports/"
//Data
t1:([Date: 2021.05.31 2021.05.31 2021.06.01 2021.06.01 2021.06.02 2021.06.02 2021.06.03 2021.06.03 2021.06.04 2021.06.04 2021.06.05 2021.06.05 2021.06.06 2021.06.06 2021.06.07 2021.06.07 2021.06.08 2021.06.08 2021.06.09 2021.06.09]
    Asset_class:`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates`FX`Rates;
    Alerts: 68 8 93 15 90 12 89 22 76 10 68 14 80 12 84 11 75 11 92 14;
    Rolling_Alerts:62 7 83 15 91 15 99 18 74 14 63 11 78 15 76 14 80 12 94 18)
table: update Date:`2021.05.31`2021.05.31`2021.06.01`2021.06.01`2021.06.02`2021.06.02`2021.06.03`2021.06.03`2021.06.04`2021.06.04`2021.06.05`2021.06.05`2021.06.06`2021.06.06`2021.06.07`2021.06.07`2021.06.08`2021.06.08`2021.06.09`2021.06.09 from t1
//dict: flip table
t2:([date:(2021.05.30 + til 5), (2021.05.30 + til 5), (2021.05.30 + til 5)]
 alert_type:`largeTrade`largeTrade`largeTrade`largeTrade`largeTrade`frontRunning`frontRunning`frontRunning`frontRunning`frontRunning`tradeCancelledAmend`tradeCancelledAmend`tradeCancelledAmend`tradeCancelledAmend`tradeCancelledAmend;
 counts: 15 14 21 19 12 14 10 10 20 19 11 20 20 18 17)
t3:([date:(2021.05.30 + til 5), (2021.05.30 + til 5), (2021.05.30 + til 5)]
 currency_pair:`AUD_USD`AUD_USD`AUD_USD`AUD_USD`AUD_USD`NZD_USD`NZD_USD`NZD_USD`NZD_USD`NZD_USD`EUR_USD`EUR_USD`EUR_USD`EUR_USD`EUR_USD;
 Alerts:41 45 45 49 47 12 11 9 13 9 10 9 10 7 4;
 Trades:9653 9638 8950 10132 10652 3329 3054 3474 2992 3551 1929 2590 1817 2357 2165)
t4:([date:(2021.05.30 + til 4), (2021.05.30 + til 4), (2021.05.30 + til 4), (2021.05.30 + til 4), (2021.05.30 + til 4)]
    alerts:`rolling`rolling`rolling`rolling`closed`closed`closed`closed`under_review`under_review`under_review`under_review`unactioned`unactioned`unactioned`unactioned`escalated`escalated`escalated`escalated;      
    counts: 73 106 116 126 68 93 90 89 1 0 2 0 5 11 6 5 1 0 2 0)
t5: select from t4 where alerts = `rolling 
t6: select from t4 where not alerts = `rolling
select from t2 where alert_type=`largeTrade
//stack 
//.qp.go[300;300] .qp.bar[select from t2 where alert_type=`frontRunning;`date;`counts; ::] 
graph_stack: {[data;x;y;grouping].qp.title["FX Alerts by Alert type"] .qp.bar[data; x; y]
                .qp.s.aes   [`fill`group; grouping, grouping]
                , .qp.s.geom  [``position`gap!(::; `stack;0.1)]}
//.qp.go[300;300] graph_stack [`t2;`date;`counts;`alert_type]
//.qp.go[300;300] .qp.line[select sum counts by date from t5; `date; `counts; ::]
//stack and line
stack_line: {[data;x;y;grouping;data2] .qp.title["Alerts Generated"] .qp.stack(
                .qp.bar[data; x; y]
                .qp.s.aes   [`fill`group; grouping, grouping]
                , .qp.s.geom  [``position`gap!(::; `stack;0.1)];
                .qp.line[?[data2;();(enlist[x]!)enlist[x];(enlist[y])!enlist(sum;y)]; x; y; ::])}  //select sum counts by date from t5
//.qp.go[300;300] stack_line [t6;`date;`counts;`alerts;t5]
//dodge and line
// Text overlay example
tab: update text:(`$"Max Value = ",/: string Alerts) from t1 where Alerts = max Alerts
dodge_line: {[data;x;y;grouping;y2;tab] .qp.title["Alerts by " , string[grouping]] .qp.stack( 
        .qp.bar[data; x; y]
        .qp.s.aes   [`fill`group; grouping,grouping]
        , .qp.s.geom  [``position`gap!(::; `dodge;0.05)]
        , .qp.s.scale[`fill; .gg.scale.colour.cat `FX`Rates`Commodities!`firebrick`black`green]; //set so colours are always the same if missing 
        .qp.line[data; x; y2]
        .qp.s.aes[`group`fill; grouping,grouping]
        , .qp.s.scale[`fill; .gg.scale.colour.cat `FX`Rates`Commodities!`firebrick`black`green];
        .qp.text[?[tab;enlist[(=;y;(max;y))];0b;()]; x; y; `text] 
          .qp.s.geom[`size`offsety`fill`bold`align!(14;-10; `firebrick;1b; `middle)]
          , .qp.s.aes[`alpha; y]
          , .qp.s.scale[`alpha; .gg.scale.alpha[50;255]])} 
//.qp.go[500;500] dodge_line [`t1;`Date;`Alerts;`Asset_class;`Rolling_Alerts;tab]
//dodge split y-axis
dodge_line_split: {[data;x;y;grouping;y2] .qp.title["Trade/Alert Count by Currency Pair"] .qp.split(
        .qp.bar[data; x; y]
        .qp.s.aes   [`fill`group; grouping,grouping]
        , .qp.s.geom  [``position`gap!(::; `dodge;0.05)]
        , .qp.s.scale [`y; .gg.scale.limits[0 0N] .gg.scale.linear];
        .qp.line[data; x; y2]
        .qp.s.aes[`group`fill; grouping,grouping])}
//.qp.go[400;300] dodge_line_split [`t3;`date;`Trades;`currency_pair;`Alerts]


// Save graphs to pdf
png_set:{[graph;val].qp.png[(`$.pdf.filePath,"/pngs/",val);800;800;.qp.theme[``canvas_fill!(::;`white)] graph]}
png_set[dodge_line [`t1;`Date;`Alerts;`Asset_class;`Rolling_Alerts;tab];"G1.png"]
png_set[graph_stack [`t2;`date;`counts;`alert_type];"G2.png"]
png_set[dodge_line_split [`t3;`date;`Trades;`currency_pair;`Alerts];"G3.png"]
png_set[stack_line [`t6;`date;`counts;`alerts;`t5];"G4.png"]
png_get:{[val].pdf.filePath,"/pngs/",val}

// Pdf wrapper functions that allow us to call the python library
//Reportlab and numpy
\d .pdf

// Python imports
saveReport.i.canvas:.p.import`reportlab.pdfgen.canvas
saveReport.i.table:.p.import[`reportlab.platypus]`:Table
saveReport.i.np:.p.import`numpy

// @kind function
// @category saveReportUtility
// @desc Convert kdb description table to printable format
// @param tab {table} kdb table to be converted
// @return {dictionary} Table and corresponding height
saveReport.i.printDescripTab:{[tab]
  dti:10&count tab;
  h:dti*27-dti%2;
  tab:value d:dti#tab;
  tab:.ml.df2tab .ml.tab2df[tab][`:round]3;
  t:enlist[enlist[`col],cols tab],key[d],'flip value flip tab;
  `h`t!(h;t)
  }

// @kind function
// @category saveReportUtility
// @desc Convert kdb table to printable format
// @param pdf {<} PDF gen module used
// @param f {int} The placement height from the bottom of the page
// @param tab {table} kdb table to be converted
// @return {int} The placement height from the bottom of the page

saveReport.i.printKdbTable:{[tab]
        dti:10&count tab;
        h:dti*27-dti%2;
        tab:0!d:dti#tab;
        //tab:.ml.df2tab .ml.tab2df[tab][`:round]3;
        t:enlist[cols tab],flip value flip tab;
        `h`t!(h;t)    
    }

// @kind function
// @category saveReportUtility
// @desc Add text to report
// @param m {<} Pdf gen module used
// @param h {int} The placement height from the bottom of the page
// @param i {int} How far below is the text
// @param txt {string} Text to include
// @param f {int} Font size
// @param s {int} Font size
// @return {int} The placement height from the bottom of the page
saveReport.i.text:{[m;h;i;txt;f;s]
  if[(h-i)<100;h:795;m[`:showPage][];];
  m[`:setFont][f;s];
  m[`:drawString][30;h-:i;txt];
  h
  }

// @kind function
// @category saveReportUtility
// @desc Add title to report
// @param m {<} Pdf gen module used
// @param h {int} The placement height from the bottom of the page
// @param i {int} How far below is the text
// @param txt {string} Text to include
// @param f {int} Font size
// @param s {int} Font size
// @return {int} The placement height from the bottom of the page
saveReport.i.title:{[m;h;i;txt;f;s]
  if[(h-i)<100;h:795;m[`:showPage][]];
  m[`:setFont][f;s];
  m[`:drawString][150;h-:i;txt];
  h
  }

// @kind function
// @category saveReportUtility
// @desc Add image to report
// @param m {<} Pdf gen module used
// @param fp {string} Filepath
// @param h {int} The placement height from the bottom of the page
// @param i {int} How far below is the text
// @param wi {int} Image width
// @param hi {int} Image height
// @return {int} The placement height from the bottom of the page
saveReport.i.image:{[m;fp;h;i;wi;hi]
  if[(h-i)<100;h:795;m[`:showPage][]];
  m[`:drawImage][fp;40;h-:i;wi;hi];
  h
  }

// @kind function
// @category saveReportUtility
// @desc Add table to report
// @param m {<} Pdf gen module used
// @param t {<} Pandas table
// @param h {int} The placement height from the bottom of the page
// @param i {int} How far below is the text
// @param wi {int} Image width
// @param hi {int} Image height
// @return {int} The placement height from the bottom of the page
saveReport.i.makeTable:{[m;t;h;i;wi;hi]
 if[(h-i)<100;h:795;m[`:showPage][]]t:saveReport.i.table 
    saveReport.i.np[`:array][t][`:tolist][];
  t[`:wrapOn][m;wi;hi];
  t[`:drawOn][m;30;h-:i];
  h
  }
\d . 

//Report Generation
alertReport_Generate:{[gg_paths;table;reportName]    
  pdf:.pdf.saveReport.i.canvas[`:Canvas] .pdf.filePath,reportName,".pdf";
  plots:gg_paths;
  f:.pdf.saveReport.i.title[pdf;775;0;"               GGPlot Example Report";"Helvetica-BoldOblique";15];
  f:.pdf.saveReport.i.image[pdf;png_get plots`Alerts_by_Asset_Class;f;500;520;480]; // space, width, height
  f:.pdf.saveReport.i.text[pdf;f;20;"The above graph is designed to show the difference in the alert count of different Asset Classes.";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"It is also built to display a rolling average for each Asset Class.";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"The table below depicts the data used in the Alerts by Asset Class Graph.";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;100;"";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"Alerts by Asset Class Table";"Helvetica-BoldOblique";11];
  ht:.pdf.saveReport.i.printKdbTable[table];
  f:.pdf.saveReport.i.makeTable[pdf;ht`t;f;ht`h;300;300]; 
  f:.pdf.saveReport.i.image[pdf;png_get plots`FX_Alerts_by_Alert_type;f;450;500;450];
  f:.pdf.saveReport.i.text[pdf;f;20;"The above graph is designed so that by calling the function with different parameters we can achieve graphs";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"displaying Alerts by Alert type for either FX or Rates. We can use the same function to produce a graph for";"Helvetica";11]; 
  f:.pdf.saveReport.i.text[pdf;f;20;"Product e.g. Spot, Swap, Forward.";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"";"Helvetica";11];
  f:.pdf.saveReport.i.image[pdf;png_get plots`Trade_Alert_Count_by_Currency_Pair;f;460;520;480];
  f:.pdf.saveReport.i.text[pdf;f;20;"The key difference between this graph and others is both y-axis are scaled differently. This allows us to";"Helvetica";11];
  f:.pdf.saveReport.i.text[pdf;f;20;"compare Alert count against Trade count.";"Helvetica";11];
  f:.pdf.saveReport.i.image[pdf;png_get plots`Alerts_Generated;f;450;520;480];
  pdf[`:save][];
  }

alertReport_Generate[gg_paths:(`Alerts_by_Asset_Class`FX_Alerts_by_Alert_type`Trade_Alert_Count_by_Currency_Pair`Alerts_Generated!("G1.png";"G2.png";"G3.png";"G4.png"));table; reportName:"Report"]
